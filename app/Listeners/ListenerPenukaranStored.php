<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Events\PenukaranStored;

class ListenerPenukaranStored
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(PenukaranStored $event)
    {
        $stokAwal   = $event->valas->stok;
        $histori    = $event->historiStok;
        if($event->jenisTukar == 'S') {
            $event->valas->decrementStok(
                $event->jumlahTukar
            );
            $histori->out = $event->jumlahTukar;
        } else {
            $event->valas->incrementStok(
                $event->jumlahTukar
            );
            $histori->in = $event->jumlahTukar;
        }
        
        $stokAkhir  = $event->valas->stok;
        
        $histori->stok_awal = $stokAwal;
        $histori->stok_akhir= $stokAkhir;
        $histori->dtm_stok  = date('Y-m-d H:i:s');
        $histori->valas()->associate($event->valas);
        $histori->save();
    }
}
