<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Valas;
use App\Historistok;

class PenukaranStored
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $valas;
    public $historiStok;

    public $jumlahTukar;
    public $jenisTukar;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        Valas $valas,
        Historistok $historiStok,
        $jumlahTukar,
        $jenisTukar
    )
    {
        $this->valas        = $valas;
        $this->historiStok  = $historiStok;
        $this->jumlahTukar  = $jumlahTukar;
        $this->jenisTukar   = $jenisTukar;
    }
}
