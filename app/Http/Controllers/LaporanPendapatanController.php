<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Penukaran;
use App\Kurs;

class LaporanPendapatanController extends Controller
{
    public function index() {
        return view('laporan.indexLaporanPendapatan');
    }
    public function postLaporan(Request $req) {
        $awal  = $req->awal;
        $akhir = $req->akhir;

        $penukaran = Penukaran::whereDate('created_at','>=',$awal)
            ->whereDate('created_at','<=',$akhir)
            ->with('detilTukar')->get();
        
        return response()->json(
            $this->cookLaporan( $penukaran )
        );
    }

    public function cetak($awal,$akhir) {
        $penukaran = Penukaran::whereDate('created_at','>=',$awal)
            ->whereDate('created_at','<=',$akhir)
            ->with('detilTukar')->get();
        $laporan = $this->cookLaporan($penukaran);
        $periode = [
            'awal' => $awal,
            'akhir'=> $akhir
        ];
        $returned = [
            'periode' => $periode,
            'laporan' => $laporan
        ];
        return view('laporan.cetakLaporanPendapatan',$returned);
    }

    private function cookLaporan($penukaran) {
        $cooked = [];

        foreach($penukaran as $tukar) {

            foreach($tukar->detilTukar as $detil) {
                $rate_transaksi = ($tukar->jenis_tukar=='S') ? $detil->jual : $detil->beli;
                $rate_modal     = ($tukar->jenis_tukar == 'S') ? $detil->modal_jual : $detil->modal_beli;
                $cooked[] = [
                    'created_at'    => $tukar->created_at->format('d-m-Y'),
                    'valas'         => $detil->valas->prefix,
                    'amount'        => $detil->pivot->amount,
                    'rate_transaksi'=> $rate_transaksi,
                    'rate_modal'    => $rate_modal,
                    'selisih'       => $detil->selisih,
                    'rupiah'        => $detil->pivot->nominal_rupiah,
                    'pendapatan'    => ($detil->selisih * $detil->pivot->amount),
                    'no_kuitansi'   => $tukar->kuitansi->no_kuitansi,
                    'jenis_transaksi'=> ($tukar->jenis_tukar == 'S') ? 'JUAL' : 'BELI',
                ];
            }

        }

        return collect($cooked);
    }
}
