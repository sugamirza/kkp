<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Penukaran;
use App\Valas;

class LaporanPenukaranController extends Controller
{
    function __construct() {

    }
    public function index() {
        return view('laporan.penukaran');
    }
    public function postLaporanPenukaran(Request $req) {
        $periode = [
            'awal' => $req->tDateAwal,
            'akhir'=> $req->tDateAkhir,
        ];
        $laporan = $this->getLaporan($req->jenis,$periode);
        return response()->json($laporan);
    }
    public function cetakLaporanPenukaran($tDateAwal,$tDateAkhir,$jenis) {
        $periode = [
            'awal'  => $tDateAwal,
            'akhir' => $tDateAkhir
        ];
        $laporan = $this->getLaporan($jenis,$periode);
        $readAbleJenis = ($jenis == 'S') ? 'PENJUALAN' : 'PEMBELIAN';
        $titleLaporan = 'LAPORAN '.$readAbleJenis.' VALAS';
        return view('laporan.viewCetakPenukaran',[
            'laporan' => $laporan,
            'titleLaporan' => $titleLaporan,
            'periode'       => $periode,
            'jenis'         => $readAbleJenis
        ]);
    }
    public function getLaporan($jenis,$periode) {
        $laporan = Penukaran::periode($periode)
            ->jenis($jenis)
            ->with('detilTukar','kuitansi')
            ->get();
        $laporanCooked = [];
        $total_amount = 0;
        
        foreach($laporan as $lap) {
            foreach($lap->detilTukar as $detil) {
                $total_amount += $detil->pivot->amount;
                $prefix = $detil->valas->prefix;
                $laporanCooked[$prefix]['data'][] = [
                    'no_kuitansi'   => $lap->kuitansi->no_kuitansi,
                    'amount'        => $detil->pivot->amount,
                    'rate'          => $detil->pivot->rate,
                ];

                if(!key_exists('jumlah_amount',$laporanCooked[$prefix])) {
                    $laporanCooked[$prefix]['jumlah_amount'] = $detil->pivot->amount;
                } else {
                    $laporanCooked[$prefix]['jumlah_amount'] += $detil->pivot->amount;
                }
            }
        }
        $laporan = collect($laporanCooked);
        return $laporan;
    }
}
