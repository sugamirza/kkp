<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kurs;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function welcome() {
        $kurses = Kurs::active(1)->with('valas')->get();
        return view('welcome', [
            'kurses' => $kurses
        ]);
    }
    public function getHome() {
        return $this->index();
    }
}
