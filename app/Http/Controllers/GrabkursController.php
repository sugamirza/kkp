<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class GrabkursController extends Controller
{
    public function grab() {
        $client = new Client;
        $res = $client->request('GET','https://www.vip.co.id/');
        $body = (string) $res->getBody();
        //echo $body;
        $this->extrakRate($body);
    }

    private function extrakRate($html) {
        $cr = new Crawler($html);
        $extrak = [];
        $tbody = $cr->filterXPath('//*[@id="rate-table"]/tbody');
        $tr = $tbody->filter('tr');
        $extrak = $tr->each(function(Crawler $node,$i) {
            return $d[] = [
                'prefix' => $node->filter('td')->eq(0)->text(),
                'beli'   => str_replace(',','',$node->filter('td')->eq(1)->text())+0,
                'jual'   => str_replace(',','',$node->filter('td')->eq(2)->text())+0
            ];
        });
        //echo $table->ownerDocument->saveHTML();
        dd($extrak);
    }
}
