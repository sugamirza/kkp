<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Valas;
use App\Historistok;

class LaporanHistoristokController extends Controller
{
    public function index() {
        $histori = Historistok::where('dtm_stok','>=','2018-01-23 00:00:00')
        ->where('dtm_stok','<=','2018-01-23 23:59:59')
        ->with('valas')->orderBy('histori_id')->get();
        $cooked = $this->cookLaporan($histori);
        
        return view('laporan.indexHistoriStok',['histori' => $cooked]);
    }

    public function postLaporan(Request $req) {
        $tDateAkhir   = $req->tDateAkhir;
        $histori      = $this->getLaporan($tDateAkhir);
        $cooked       = $this->cookLaporan($histori);

        return response()->json($cooked);
    }
    public function fetchDatasets(Request $req) {
        
        $akhir = $req->tDateAkhir;
        $awal  = $req->tDateAwal;

        $histori = Historistok::whereDate('dtm_stok','>=',$awal)->whereDate('dtm_stok','<=',$akhir)->get();

        $masterLabel = [];
        $datasets = [];

        foreach($histori as $his) {
            $finded = $this->findPrefix($datasets,$his->valas->prefix);
            if($finded['find']) {
                $index = $finded['index'];
                array_push($datasets[$index]['data'], $his->stok_akhir);
            }
            else {
                $newDs = [
                    'label' => $his->valas->prefix,
                    'data' => [ $his->stok_akhir ],
                    'borderColor' => $this->random_color(),
                    'fill'  => false
                ];
                array_push($datasets,$newDs);
            }
            if(!in_array($his->dtm_stok->format('d H:i').' WIB',$masterLabel) ) {
                array_push($masterLabel,$his->dtm_stok->format('d H:i').' WIB');
            }
        }
        return response()->json([
            'datasets' => $datasets,
            'labels' => $masterLabel
        ]);
    }

    private function findPrefix($data,$label) {
        if(count($data) <= 0) {
            return $ret = ['find' => false, 'index' => null];
        }
        for($i=0; $i < count($data); $i++) {
            if($data[$i]['label'] == $label) {
                
                return $ret = ['find' => true, 'index' => $i];
            }
        }
        return $ret = ['find' => false, 'index' => null];
    }
    private function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }
    
    private function random_color() {
        return '#'.$this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    
    public function cetak($akhir) {
        $periode = [
            'akhir' => $akhir
        ];
        $histori = $this->cookLaporan( $this->getLaporan($akhir) );
        return view('laporan.cetakLaporanHistoristok',[
            'periode' => $periode,
            'histori' => $histori
        ]);
    }
    

    private function getLaporan($tDateAkhir) {
        $histori    = Historistok:://whereDate('dtm_stok','>=',$tDateAwal)
            whereDate('dtm_stok','<=',$tDateAkhir)
            ->with('valas')->orderBy('histori_id')->get();
        return $histori;
    }

    private function cookLaporan($histori) {
        $cooked = [];

        foreach($histori as $h) {
            $prefix = $h->valas->prefix;

            if( isset($cooked[$prefix] )) {
                $cooked[$prefix]['total_in'] += $h->in;
                $cooked[$prefix]['total_out'] += $h->out;
                $cooked[$prefix]['stokAwal'][] = $h->stok_awal;
                $cooked[$prefix]['stokAkhir'][] = $h->stok_akhir;
            }
            else {
                $cooked[$prefix] = [
                    'total_in' => $h->in,
                    'total_out'=> $h->out
                ];
                $cooked[$prefix]['stokAwal'][] = $h->stok_awal;
                $cooked[$prefix]['stokAkhir'][] = $h->stok_akhir;
            }
            //echo "Nama Valas:".$h->valas->prefix." ". $h->dtm_stok." |IN ".$h->in."|<br>";
        }
        $cooked2 = [];
        foreach($cooked as $key => $c) {
            $cooked[$key]['stokAwal']   = $cooked[$key]['total_in'];

            $cooked[$key]['stokAkhir']  = $cooked[$key]['total_in'] - $cooked[$key]['total_out'];
        }
        return $cooked;
    }
}
