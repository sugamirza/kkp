<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

use App\Mitra;
use App\Kurs;
use App\Valas;
class EntryKursMitraController extends Controller
{
    protected $kurs = [];
    protected $tBody;
    public $cr;
    function __construct() {
        $client = new Client;
        $res = $client->request('GET','https://www.vip.co.id/');
        $body = (string) $res->getBody();
        $this->cr = new Crawler($body);
    }
    public function index() {
        return view('EntryKursMitra.index');
    }
    public function simpan(Request $req) {
        $mitra = Mitra::find($req->mitraId);
        $selisih = $req->selisih;
        foreach($req->valases as $valas) {
            if($mitra->kurs->count() < 1) {
                $kurs = new Kurs;
                $objvalas = Valas::find($valas);
                if($objvalas->count() > 0) {
                    $rate = $this->extrakRate( $objvalas->prefix );
                    if($rate !== null) {
                        $kurs->valas()->associate($valas);
                        $kurs->mitra()->associate($mitra);
                        $kurs->modal_jual = $rate['jual'];
                        $kurs->modal_beli = $rate['beli'];
                        $kurs->selisih    = $selisih;
                        $kurs->jual       = $rate['jual'] + $selisih;
                        $kurs->beli       = $rate['beli'] + $selisih;
                        $kurs->is_active  = false;
                        $kurs->save();
                    } else { }
                } else {

                }
            } else {
                foreach($mitra->kurs as $mitkurs) {
                    if($mitkurs->valas_id !== $valas) {
                        $kurs = new Kurs;
                        $objvalas = Valas::find($valas);
                        $rate = $this->extrakRate( $objvalas->prefix );
                        if($rate !== null) {
                            $kurs->valas()->associate($valas);
                            $kurs->mitra()->associate($mitra);
                            $kurs->modal_jual = $rate['jual'];
                            $kurs->modal_beli = $rate['beli'];
                            $kurs->jual       = $rate['jual'] + $selisih;
                            $kurs->beli       = $rate['beli'] + $selisih;
                            $kurs->selisih    = $selisih;
                            $kurs->is_active  = false;
                            $kurs->save();
                        } else {
                        }
                    } else {
                        $rate = $this->extrakRate( $mitkurs->valas->prefix );
                        $mitkurs->modal_jual = $rate['jual'];
                        $mitkurs->modal_beli = $rate['beli'];
                        $mitkurs->jual       = $rate['jual'] + $selisih;
                        $mitkurs->beli       = $rate['beli'] + $selisih;
                        $mitkurs->selisih    = $selisih;
                        $mitkurs->save();
                    }

                }
            }
        }
        $mitra = Mitra::find($req->mitraId)->first();
        $valaskurs = [];
        foreach($mitra->kurs as $kurs) {
            $valaskurs[] = [
                'prefix' => $kurs->valas->prefix,
                'modal_jual' => $kurs->modal_jual,
                'modal_beli' => $kurs->modal_beli,
            ];
        }
        return response()->json([
            'kurs' => $mitra->kurs,
            'valas'=> $valaskurs,
            'status' => 200
        ]);
    }

    public function settingKurs() {
        $mitra = Mitra::get();
        return view('EntryKursMitra.setting',['mitra' => $mitra]);
    }
    public function simpanSeting(Request $req) {
        $mitraid = $req->activKurs;
        $mitra = Mitra::find($mitraid);
        $kurs = Kurs::where('is_active',1)->get();
        foreach($kurs as $k) {
            $k->is_active = 0;
            $k->save();
        }
        foreach($mitra->kurs as $kurs) {
            $kurs->is_active = 1;
            $kurs->save();
        }
        return redirect(route('settingKurs'));
    }

    private function grab() {

        // $cr = new Crawler($body);
        // $this->tbody = $cr->filterXPath('//*[@id="rate-table"]/tbody');
        //$tr = $tbody->filter('tr');

        //$this->extrakRate($body);
    }
    private function extrakRate($prefix) {
        
        $tBody = $this->cr->filterXPath('//*[@id="rate-table"]/tbody');
        $tr = $tBody->filter('tr');
        $extrak = $tr->each(function(Crawler $node,$i) {
            return $d[] = [
                'prefix' => $node->filter('td')->eq(0)->text(),
                'beli'   => str_replace(',','',$node->filter('td')->eq(1)->text())+0,
                'jual'   => str_replace(',','',$node->filter('td')->eq(2)->text())+0
            ];
        });
        foreach($extrak as $k => $e) {
            if($e['prefix'] == $prefix) {
                //die(json_encode($extrak[$k]));
                return $extrak[$k];
            }
        }
        //echo $table->ownerDocument->saveHTML();
        return $extrak = null;
        //return $extrak;
    }
}
