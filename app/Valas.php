<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Historistok;

class Valas extends Model
{
    protected $primaryKey = 'valas_id';
    protected $table = 'valas';
    protected $fillable = [
        'nama_valas',
        'prefix',
        'deskripsi',
        'stok'
    ];
    public $timestamps = false;

    public function search($q) {
        return $this->where('prefix','like',$q.'%')->get();
    }
    public function kurses() {
        return $this->hasMany(Kurs::class,'valas_id');
    }
    public function activeKurs() {
        return $this->kurses()->where('is_active',1)->first();
    }
    public function bbsv() {
        return $this->belongsToMany(Bbsv::class,'detil_bbsv','valas_id','bbsv_id');
    }
    public function mitra() {
        //return $this->hasManyThrough(Mitra::class,'')
    }

    public function historiStok() {
        return $this->hasMany(Historistok::class,'valas_id');
    }

    public static function batchIncrementStok( $valas ) {
        foreach($valas as $item) {
            $valas = self::find($item['valas_id']);

            $historiStok    = new Historistok;
            $stokAwal       = $valas->stok;
            $stokAkhir      = $stokAwal + $item['stok'];

            $valas->incrementStok($item['stok']);
            
            $historiStok->stok_awal = $stokAwal;
            $historiStok->stok_akhir= $stokAkhir;
            $historiStok->in        = $item['stok'];
            $historiStok->dtm_stok  = date('Y-m-d H:i:s');

            $historiStok->valas()->associate( $valas->valas_id );
            $historiStok->save();
        }
        return true;
    }

    public function incrementStok( $stok ) {
        $this->stok += $stok;
        if( $this->save() ) {
            return true;
        }
        return false;
    }
    public function decrementStok( $stok ) {
        $this->stok -= $stok;
        return $this->save();
    }
}
