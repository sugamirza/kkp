<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historistok extends Model
{
    protected $table = 'histori_stok';
    protected $fillable = [
        'stok_awal',
        'stok_akhir',
        'dtm_stok',
        'in',
        'out'
    ];
    protected $dates = [
        'dtm_stok',
    ];
    public $timestamps = false;

    public function valas() {
        return $this->belongsTo(Valas::class,'valas_id');
    }
}
