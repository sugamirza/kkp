@extends('layouts.dashboard')
@section('title','Dashboard - ENTRY KURS MITRA')
@section('content')
  
  
  <div class="panel panel-default">
    <div class="panel-body">
       Basic panel example
  
  
  <form action="{{ route('simpanSeting') }}" method="POST" role="form">
    <legend>Setting Kurs</legend>
    <div class="form-group">
    <button type="submit" class="btn btn-primary">Simpan Setting</button>
    </div>
    {{ csrf_field() }}
  <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>Active</th>
        <th>Nama Mitra</th>
        <th>
          <table class="table table-bordered">
            <tr>
              KURS
            </tr>
            <tr>
              <td>Jual</td>
              <td>Beli</td>
            </tr>
          </table>
        </th>
      </tr>
    </thead>
    <tbody>
      @foreach($mitra as $m)
        <tr>
          <td>
          <div class="radio">
            <label>
              <input type="radio" name="activKurs" value="{{ $m->mitra_id }}">
              active
            </label>
          </div>
          </td>
          <td>{{ $m->nama }}</td>
          <td>
            <table class="table table-bordered table-hover">
              <thead>
                  <th>Jual</th>
                  <th>Beli</th>
              </thead>
              <tbody>
              @foreach($m->kurs as $kurs)
              <tr>
                <td>{{ $kurs->modal_jual }}</td>
                <td>{{ $kurs->modal_beli }}</td>
              </tr>
              @endforeach
              </tbody>
            </table>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</form>
  </div>
  </div>
@endsection