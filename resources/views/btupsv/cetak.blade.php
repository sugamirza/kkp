<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet"  href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" media="print" type="text/css"  href="{{ mix('css/app.css') }}">
    <script src="{{ mix('js/print.min.js') }}"></script>
    <title>Bukti Terima Uang Pembelian Stok Valas</title>
</head>
<body id="cetak">
    <div id="print" class="container">
        <button type="button" class="btn btn-default" onclick="printJS({ printable: 'cetak', type: 'html',showModal:true })">button</button>
        
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="kontent_cetak">
                    <div class="kop">
                        <img media="print" src="{{ URL::asset('images/Logo.png') }}" alt="">
                    </div>
                    <div class="container">
                        <div class="col-lg-12 body_cetak">
                            <h3 class="text-center"><strong>BUKTI TERIMA UANG PEMBELIAN STOK VALAS</strong></h3>
                            <hr>
                            <div class="row body_heading">
                            
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <p>No: {{ $btupsv->btupsv_id }}/{{ date('Y') }};</p>
                                </div>
                                <div class="col-xs-8 col-sm-6 col-md-8 col-lg-8">
                                    <p class="pull-right">Jakart, {{ date('d-m-Y') }}</p>
                                </div>
                            </div>
                                <p align="left" style="padding-left:30px;">
                                    Berikut ini adalah Bukti Terima Uang Pembelian Stok Valas sejumlah <b>Rp, {{ $ppsv->total_rupiah }}</b> yang telah diserahkan kepada staf pembelian pada tanggal {{ date('Y-m-d') }}.
                                    <br>

                                    Dengan detil sebagai berikut :
                                </p>
                                
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>VALAS</th>
                                            <th>JUMLAH</th>
                                            <th>KURS</th>
                                            <th>RUPIAH</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ppsv->detilPpsv as $detil)
                                        <tr>
                                            <td>{{ $detil->valas->prefix }}</td>
                                            <td>{{ $detil->pivot->amount }}</td>
                                            <td>{{ $detil->pivot->rate }}</td>
                                            <td>{{ $detil->pivot->nominal_rupiah }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-lg-12">
                            <div class="col-lg-6 text-center">
                                Teller,
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <p>( {{ $btupsv->teller->nama_user }} )</p>
                            </div>
                            <div class="col-lg-6 text-center">
                                Staf Pembelian,
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <p>( {{ $btupsv->receiver->nama_user }} )</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</body>
</html>