<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet"  href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" media="print" type="text/css"  href="{{ mix('css/app.css') }}">
    <script src="{{ mix('js/print.min.js') }}"></script>
    <title>{{ $titleLaporan }}</title>
</head>
<body>
  <button type="button" class="btn btn-default" onClick="PrintImg()">Cetak</button>
    <div id="cetak" class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="kontent_cetak">
                    <div class="kop col-lg-12">
                      <img id="logo" class="group-media" media="print" src="{{ URL::asset('images/Logo.png') }}" alt="">
                    </div>
                    <div class="container">
                        <div class="col-lg-12 body_cetak">
                            <h3 class="text-center"><strong>{{ $titleLaporan }}</strong></h3>
                            <hr>
                            <div class="row body_heading">
                              <p>Berikut ini adalah laporan {{$jenis}} valas pertanggal <?php echo ($periode['awal'] == $periode['akhir']) ? "<b>".$periode['akhir']."</b>":"<b>".$periode['awal']."</b> sampai dengan <b>".$periode['akhir']."</b>"; ?></p>
                              <br>
                            </div>
                                <p align="left" style="padding-left:30px;">
                                    
                                </p>
                                
                                <div class="row">
                                  <div class="col-lg-offset-1 col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                  <table class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <th>VALAS</th>
                                      <th width="180">NOMOR KUITANSI</th>
                                      <th>AMOUNT</th>
                                      <th>TOTAL</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php $total_amount=0; ?>
                                  @foreach($laporan as $key => $lap)
                                    <tr>
                                      <td class="text-center">{{ $key }}</td>
                                      
                                      <td style="padding:0px !important;">
                                        
                                        <table class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                          @foreach($lap['data'] as $det)
                                          <tr v-for="det in lap.data">
                                            <td style="border:1px solid #ddd !important;">{{ $det['no_kuitansi'] }}</td>
                                          </tr>
                                          @endforeach
                                        </table>
                                      </td>
                                      <td style="padding:0px !important;">
                                        <table class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                          @foreach($lap['data'] as $det)
                                          <tr v-for="det in lap.data">
                                            <td style="border:1px solid #ddd !important;">{{ $det['amount'] }}</td>
                                          </tr>
                                          @endforeach
                                        </table>
                                      </td>
                                      <td class="text-center"> {{$lap['jumlah_amount']}} </td>
                                      <?php $total_amount +=$lap['jumlah_amount']; ?>
                                    </tr>
                                  @endforeach
                                    <tr>
                                      <td colspan="3"></td>
                                      <td class="text-center"><h3>{{ $total_amount }}</h3></td>
                                    </tr>
                                  </tbody>
                                </table>
                                  </div>
                                </div>

                                <p>Demikian Laporan Pembelian Valas ini kami buat, terima kasih.</p>
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-lg-12 footer_cetak">
                            <div class="col-lg-6">
                                <p>Mengetahui,</p>
                                <br>
                                <br>
                                <br>
                                <br>
                                <p>( {{ Auth::user()->nama_user }} )</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
    <script>
    function PrintImg() {
      var css = '@page { size:potrait }',
          head = document.head || document.getElementsByTagName('head')[0],
          style = document.createElement('style');

      style.type = 'text/css';
      style.media = 'print';

      if (style.styleSheet){
        style.styleSheet.cssText = css;
      } else {
        style.appendChild(document.createTextNode(css));
      }

      head.appendChild(style);

      window.print();
    }
    </script>
</body>
</html>