<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet"  href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" media="print" type="text/css"  href="{{ mix('css/app.css') }}">
    <script src="{{ mix('js/print.min.js') }}"></script>
    <title>LAPORAN PENDAPATAN</title>
</head>
<body>
<style>
  @media print {
      .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
      .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
           float: left;               
      }

      .col-sm-12 {
           width: 100%;
      }

      .col-sm-11 {
           width: 91.66666666666666%;
      }

      .col-sm-10 {
           width: 83.33333333333334%;
      }

      .col-sm-9 {
            width: 75%;
      }

      .col-sm-8 {
            width: 66.66666666666666%;
      }

       .col-sm-7 {
            width: 58.333333333333336%;
       }

       .col-sm-6 {
            width: 50%;
       }

       .col-sm-5 {
            width: 41.66666666666667%;
       }
  }
</style>
  <button type="button" class="btn btn-default" onClick="PrintImg()">Cetak</button>
    <div id="cetak" class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="kontent_cetak">
                    <div class="kop col-lg-12">
                      <img id="logo" class="group-media" media="print" src="{{ URL::asset('images/Logo.png') }}" alt="">
                    </div>
                    <div class="container">
                        <div class="col-lg-12">
                            <h3 class="text-center" align="center"><strong>LAPORAN PENDAPATAN</strong></h3>
                            <hr>
                            <div class="row body_heading">
                            <p>Berikut ini adalah Laporan Pendapatan pertanggal <?php echo ($periode['awal'] == $periode['akhir']) ? "<b>".$periode['akhir']."</b>":"<b>".$periode['awal']."</b> sampai dengan <b>".$periode['akhir']."</b>"; ?></p>
                              <br>
                            </div>
                                <p align="left" style="padding-left:30px;">
                                    
                                </p>
                                
                                <div class="row">
                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                  <table class="table table-bordered table-hover">
                                  <thead>
                                    <tr>
                                      <th>TANGGAL</th>
                                      <th>NO KUITANSI</th>
                                      <th>VALAS</th>
                                      <th>JENIS TRANSAKSI</th>
                                      <th>AMOUNT</th>
                                      <th>KURS TRANSAKSI</th>
                                      <th>KURS MODAL</th>
                                      <th>SELISIH</th>
                                      <th>TOTAL PENDAPATAN</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php $total_pendapatan=0; $total_amount=0; ?>
                                    @foreach($laporan as $val)
                                      <tr>
                                        <td>{{ $val['created_at'] }}</td>
                                        <td>{{ $val['no_kuitansi'] }}</td>
                                        <td>{{ $val['valas'] }}</td>
                                        <td>{{ $val['jenis_transaksi'] }}</td>
                                        <td>{{ $val['amount'] }}</td>
                                        <td>{{ $val['rate_transaksi'] }}</td>
                                        <td>{{ $val['rate_modal'] }}</td>
                                        <td>{{ $val['selisih'] }}</td>
                                        <td>{{ $val['pendapatan'] }}</td>
                                      </tr>
                                      <?php $total_pendapatan += $val['pendapatan']; $total_amount += $val['amount']; ?>
                                    @endforeach
                                    <tr>
                                      <td colspan="3"></td>
                                      <td><b>TOTAL AMOUNT</b></td>
                                      <td><b><?php echo formatRp($total_amount); ?></b></td>
                                      <td colspan="2"></td>
                                      <td><b>TOTAL PENDAPATAN</b></td>
                                      <td><b>Rp, <?php echo formatRp($total_pendapatan); ?></b></td>
                                    </tr>
                                  </tbody>
                                </table>
                                  </div>
                                </div>

                                <p>Demikian Laporan Pendapatan ini kami buat, terima kasih.</p>
                        </div>
                    </div>
                    <div class="container">
                        <div class="col-lg-12 footer_cetak">
                            <div class="col-lg-6">
                                <p>Mengetahui,</p>
                                <br>
                                <br>
                                <br>
                                <br>
                                <p>( {{ Auth::user()->nama_user }} )</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
    <script>
    function PrintImg() {
      var css = '@page { size: landscape; }',
          head = document.head || document.getElementsByTagName('head')[0],
          style = document.createElement('style');

      style.type = 'text/css';
      style.media = 'print';

      if (style.styleSheet){
        style.styleSheet.cssText = css;
      } else {
        style.appendChild(document.createTextNode(css));
      }

      head.appendChild(style);

      window.print();
    }
    </script>

<?php
function formatRp($nilai) {
  return number_format($nilai,0,',','.');
}
?>
</body>
</html>